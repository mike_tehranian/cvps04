import cv2

import experiment
import ps4


class EdgeFinder:
    def __init__(self, base_img, motion_img, filter_size=1, threshold1=0,
                 threshold2=0, threshold3=0):
        self._base_img = base_img
        self._motion_img = motion_img
        self._filter_size = filter_size
        self._threshold1 = threshold1
        self._threshold2 = threshold2
        self._threshold3 = threshold3

        #def onchangeThreshold1(pos):
            #self._threshold1 = pos
            #self._render()

        def onchangeThreshold2(pos):
            self._threshold2 = pos
            self._render()

        def onchangeThreshold3(pos):
            self._threshold3 = pos
            self._render()

        def onchangeFilterSize(pos):
            self._filter_size = pos
            #self._filter_size += (self._filter_size + 1) % 2
            self._render()

        cv2.namedWindow('edges')

        cv2.createTrackbar('filter_size', 'edges', self._filter_size, 200, onchangeFilterSize)
        #cv2.createTrackbar('threshold1', 'edges', self._threshold1, 255, onchangeThreshold1)
        cv2.createTrackbar('threshold2', 'edges', self._threshold2, 25, onchangeThreshold2)
        cv2.createTrackbar('threshold3', 'edges', self._threshold3, 255, onchangeThreshold3)

        self._render()

        print "Adjust the parameters as desired.  Hit any key to close."

        cv2.waitKey(0)

        cv2.destroyWindow('edges')
        cv2.destroyWindow('smoothed')

    def threshold1(self):
        return self._threshold1

    def threshold2(self):
        return self._threshold2

    def threshold3(self):
        return self._threshold3

    def filterSize(self):
        return self._filter_size

    def quiver(self):
        return self._quiver

    #Jdef smoothedImage(self):
        #Jreturn self._smoothed_img

    def _render(self):
        #self._smoothed_img = cv2.GaussianBlur(self.image, (self._filter_size, self._filter_size), sigmaX=0, sigmaY=0)
        #self._edge_img = cv2.Canny(self._smoothed_img, self._threshold1, self._threshold2)

        #u, v = ps4.optic_flow_lk(self._base_img,
                                 #self._motion_img,
                                 #self._filter_size, self._threshold1, self._threshold2)

        interpolation = cv2.INTER_CUBIC  # You may try different values
        border_mode = cv2.BORDER_REFLECT101  # You may try different values
        u, v = ps4.hierarchical_lk(self._base_img, self._motion_img,
                                self._threshold2, self._filter_size, 'uniform',
                                self._threshold3, interpolation, border_mode)

        # Flow image
        self._quiver = experiment.quiver(u, v, scale=3, stride=10)

        #cv2.imshow('smoothed', self._smoothed_img)
        cv2.imshow('quiver', self._quiver)
