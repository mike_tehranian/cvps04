"""Problem Set 4: Motion Detection"""

import numpy as np
import cv2
import os


# Utility function
def normalize_and_scale(image_in, scale_range=(0, 255)):
    """Normalizes and scales an image to a given range [0, 255].

    Utility function. There is no need to modify it.

    Args:
        image_in (numpy.array): input image.
        scale_range (tuple): range values (min, max). Default set to
                             [0, 255].

    Returns:
        numpy.array: output image.
    """
    image_out = np.zeros(image_in.shape)
    cv2.normalize(image_in, image_out, alpha=scale_range[0],
                  beta=scale_range[1], norm_type=cv2.NORM_MINMAX)

    return image_out


# Assignment code
def gradient_x(image):
    """Computes image gradient in X direction.

    Use cv2.Sobel to help you with this function. Additionally you
    should set cv2.Sobel's 'scale' parameter to one eighth and ksize
    to 3.

    Args:
        image (numpy.array): grayscale floating-point image with
                             values in [0.0, 1.0].

    Returns:
        numpy.array: image gradient in the X direction. Output
                     from cv2.Sobel.
    """
    # When ddepth=-1, the destination image will have the same depth as the source
    ddepth = cv2.CV_64F # or -1
    gradient_x = cv2.Sobel(image, ddepth, 1, 0, ksize=3, scale=1/8.0)

    return gradient_x


def gradient_y(image):
    """Computes image gradient in Y direction.

    Use cv2.Sobel to help you with this function. Additionally you
    should set cv2.Sobel's 'scale' parameter to one eighth and ksize
    to 3.

    Args:
        image (numpy.array): grayscale floating-point image with
                             values in [0.0, 1.0].

    Returns:
        numpy.array: image gradient in the Y direction.
                     Output from cv2.Sobel.
    """
    # When ddepth=-1, the destination image will have the same depth as the source
    ddepth = cv2.CV_64F # or -1
    gradient_y = cv2.Sobel(image, ddepth, 0, 1, ksize=3, scale=1/8.0)

    return gradient_y


def optic_flow_lk(img_a, img_b, k_size, k_type, sigma=1):
    """Computes optic flow using the Lucas-Kanade method.

    For efficiency, you should apply a convolution-based method.

    Note: Implement this method using the instructions in the lectures
    and the documentation.

    You are not allowed to use any OpenCV functions that are related
    to Optic Flow.

    Args:
        img_a (numpy.array): grayscale floating-point image with
                             values in [0.0, 1.0].
        img_b (numpy.array): grayscale floating-point image with
                             values in [0.0, 1.0].
        k_size (int): size of averaging kernel to use for weighted
                      averages. Here we assume the kernel window is a
                      square so you will use the same value for both
                      width and height.
        k_type (str): type of kernel to use for weighted averaging,
                      'uniform' or 'gaussian'. By uniform we mean a
                      kernel with the only ones divided by k_size**2.
                      To implement a Gaussian kernel use
                      cv2.getGaussianKernel. The autograder will use
                      'uniform'.
        sigma (float): sigma value if gaussian is chosen. Default
                       value set to 1 because the autograder does not
                       use this parameter.

    Returns:
        tuple: 2-element tuple containing:
            U (numpy.array): raw displacement (in pixels) along
                             X-axis, same size as the input images,
                             floating-point type.
            V (numpy.array): raw displacement (in pixels) along
                             Y-axis, same size and type as U.
    """

    if k_size and k_type:
        if k_type == 'gaussian':
            kernel = cv2.getGaussianKernel(k_size, sigma)
        elif k_type == 'uniform':
            kernel = np.ones((k_size, k_size), np.float64) / (k_size ** 2)
    else:
        print "Warning: Using Default Kernel"
        kernel = np.ones((5, 5), np.float64) / (5 ** 2)

    I_x = gradient_x(img_a)
    I_y = gradient_y(img_a)
    I_t = img_b - img_a
    I_x_2 = I_x ** 2
    I_y_2 = I_y ** 2

    # The output image will have the same depth as the source
    ddepth = -1
    sum_I_x_x = cv2.filter2D(I_x_2, ddepth, kernel)
    sum_I_y_y = cv2.filter2D(I_y_2, ddepth, kernel)
    sum_I_x_y = cv2.filter2D(I_x * I_y, ddepth, kernel)
    sum_I_x_t = cv2.filter2D(I_x * I_t, ddepth, kernel)
    sum_I_y_t = cv2.filter2D(I_y * I_t, ddepth, kernel)

    # A is 2x2 matrix with each element being a M x N matrix
    # A_inverse = (1 / (ad - bc)) * [d -b; -c a]
    # A_inverse = (1 / (ad - bc)) * [sum_I_y_y -sum_I_x_y; -sum_I_x_y sum_I_x_x]
    determinant_img_a = (sum_I_x_x * sum_I_y_y) - (sum_I_x_y * sum_I_x_y)

    # Pixels that have singular matrices
    singular_pixels = np.where(np.absolute(determinant_img_a) <= 1e-12)

    # Set pixels that are non-solvable to really low values in U & V
    determinant_img_a[singular_pixels] = float('inf')
    #determinant_img_a[singular_pixels] = 0.0001

    # Solution
    # 1st row (U) = (1 / (ad - bc)) * [d*e + -b*f]
    # 2nd row (V) = (1 / (ad - bc)) * [-c*e + a*f]
    U = (sum_I_y_y * -sum_I_x_t + -sum_I_x_y * -sum_I_y_t) / determinant_img_a
    V = (-sum_I_x_y * -sum_I_x_t + sum_I_x_x * -sum_I_y_t) / determinant_img_a

    return U, V


def reduce_image(image):
    """Reduces an image to half its shape.

    The autograder will pass images with even width and height. It is
    up to you to determine values with odd dimensions. For example the
    output image can be the result of rounding up the division by 2:
    (13, 19) -> (7, 10)

    For simplicity and efficiency, implement a convolution-based
    method using the 5-tap separable filter.

    Follow the process shown in the lecture 6B-L3. Also refer to:
    -  Burt, P. J., and Adelson, E. H. (1983). The Laplacian Pyramid
       as a Compact Image Code
    You can find the link in the problem set instructions.

    Args:
        image (numpy.array): grayscale floating-point image, values in
                             [0.0, 1.0].

    Returns:
        numpy.array: output image with half the shape, same type as the
                     input image.
    """
    five_tap_filter = np.array([[1, 4, 6, 4, 1]]) / 16.0
    filtered_image = cv2.sepFilter2D(image, -1, five_tap_filter, five_tap_filter.T)
    subsample_image = filtered_image[::2, ::2]

    return subsample_image


def gaussian_pyramid(image, levels):
    """Creates a Gaussian pyramid of a given image.

    This method uses reduce_image() at each level. Each image is
    stored in a list of length equal the number of levels.

    The first element in the list ([0]) should contain the input
    image. All other levels contain a reduced version of the previous
    level.

    All images in the pyramid should floating-point with values in

    Args:
        image (numpy.array): grayscale floating-point image, values
                             in [0.0, 1.0].
        levels (int): number of levels in the resulting pyramid.

    Returns:
        list: Gaussian pyramid, list of numpy.arrays.
    """
    subsampled_image = np.copy(image)
    pyramid = [subsampled_image]

    for _ in range(levels - 1):
        subsampled_image = reduce_image(subsampled_image)
        pyramid.append(subsampled_image)

    # assert len(pyramid) == levels
    return pyramid


def create_combined_img(img_list):
    """Stacks images from the input pyramid list side-by-side.

    Ordering should be large to small from left to right.

    See the problem set instructions for a reference on how the output
    should look like.

    Make sure you call normalize_and_scale() for each image in the
    pyramid when populating img_out.

    Args:
        img_list (list): list with pyramid images.

    Returns:
        numpy.array: output image with the pyramid images stacked
                     from left to right.
    """
    largest_image = img_list[0]
    combined_height = largest_image.shape[0]
    combined_image = normalize_and_scale(largest_image)

    for image in img_list[1:]:
        rows, cols = image.shape

        # Pad the image with extra rows to match the largest (first) image
        padding = np.zeros((combined_height - rows, cols))
        padded_image = np.vstack((normalize_and_scale(image), padding))

        # Stack the image to the right of the combined stack
        combined_image = np.hstack((combined_image, padded_image))

    return combined_image


def expand_image(image):
    """Expands an image doubling its width and height.

    For simplicity and efficiency, implement a convolution-based
    method using the 5-tap separable filter.

    Follow the process shown in the lecture 6B-L3. Also refer to:
    -  Burt, P. J., and Adelson, E. H. (1983). The Laplacian Pyramid
       as a Compact Image Code

    You can find the link in the problem set instructions.

    Args:
        image (numpy.array): grayscale floating-point image, values
                             in [0.0, 1.0].

    Returns:
        numpy.array: same type as 'image' with the doubled height and
                     width.
    """
    output_rows, output_cols = image.shape[0]*2, image.shape[1]*2
    padded_image = np.zeros((output_rows, output_cols))
    padded_image[: :2, : :2] = image

    five_tap_filter = np.array([1, 4, 6, 4, 1]) / 8.0
    expanded_image = cv2.sepFilter2D(padded_image, -1, five_tap_filter, five_tap_filter.T)

    return expanded_image


def laplacian_pyramid(g_pyr):
    """Creates a Laplacian pyramid from a given Gaussian pyramid.

    This method uses expand_image() at each level.

    Args:
        g_pyr (list): Gaussian pyramid, returned by gaussian_pyramid().

    Returns:
        list: Laplacian pyramid, with l_pyr[-1] = g_pyr[-1].
    """
    pyramid_levels = []

    for level in range(len(g_pyr) - 1):
        gauss_level_num_rows = g_pyr[level].shape[0]
        gauss_level_num_cols = g_pyr[level].shape[1]

        # Upsample the next level (smaller) image to this size
        upsampled_image = expand_image(g_pyr[level + 1])

        # Crop the upsampled image to the size of the Gaussian
        upsampled_image_cropped = \
                upsampled_image[:gauss_level_num_rows, :gauss_level_num_cols]
        lapl_image = g_pyr[level] - upsampled_image_cropped
        pyramid_levels.append(lapl_image)

    # l_n = g_n
    pyramid_levels.append(g_pyr[-1])

    return pyramid_levels


def warp(image, U, V, interpolation, border_mode):
    """Warps image using X and Y displacements (U and V).

    This function uses cv2.remap. The autograder will use cubic
    interpolation and the BORDER_REFLECT101 border mode. You may
    change this to work with the problem set images.

    See the cv2.remap documentation to read more about border and
    interpolation methods.

    Args:
        image (numpy.array): grayscale floating-point image, values
                             in [0.0, 1.0].
        U (numpy.array): displacement (in pixels) along X-axis.
        V (numpy.array): displacement (in pixels) along Y-axis.
        interpolation (Inter): interpolation method used in cv2.remap.
        border_mode (BorderType): pixel extrapolation method used in
                                  cv2.remap.

    Returns:
        numpy.array: warped image, such that
                     warped[y, x] = image[y + V[y, x], x + U[y, x]]
    """
    rows, cols = image.shape
    x_v, y_v = np.meshgrid(range(cols), range(rows))

    map_x = (x_v + U).astype(np.float32)
    map_y = (y_v + V).astype(np.float32)

    warped_image = cv2.remap(image, map_x, map_y, interpolation, borderMode=border_mode)

    return warped_image


def hierarchical_lk(img_a, img_b, levels, k_size, k_type, sigma, interpolation,
                    border_mode):
    """Computes the optic flow using Hierarchical Lucas-Kanade.

    This method should use reduce_image(), expand_image(), warp(),
    and optic_flow_lk().

    Args:
        img_a (numpy.array): grayscale floating-point image, values in
                             [0.0, 1.0].
        img_b (numpy.array): grayscale floating-point image, values in
                             [0.0, 1.0].
        levels (int): Number of levels.
        k_size (int): parameter to be passed to optic_flow_lk.
        k_type (str): parameter to be passed to optic_flow_lk.
        sigma (float): parameter to be passed to optic_flow_lk.
        interpolation (Inter): parameter to be passed to warp.
        border_mode (BorderType): parameter to be passed to warp.

    Returns:
        tuple: 2-element tuple containing:
            U (numpy.array): raw displacement (in pixels) along X-axis,
                             same size as the input images,
                             floating-point type.
            V (numpy.array): raw displacement (in pixels) along Y-axis,
                             same size and type as U.
    """
    a_gaussian_pyr = gaussian_pyramid(img_a, levels)
    b_gaussian_pyr = gaussian_pyramid(img_b, levels)

    # Initialize the Flow to zero for all pixels
    U = np.zeros_like(a_gaussian_pyr[levels - 1])
    V = np.zeros_like(a_gaussian_pyr[levels - 1])

    for i in reversed(range(levels)):
        # Upsample the flow
        U_upsampled = expand_image(U) * 2
        V_upsampled = expand_image(V) * 2

        # Predicted Flow must match Gaussian level dimensions
        rows, cols = a_gaussian_pyr[i].shape
        U_p = U_upsampled[:rows, :cols]
        V_p = V_upsampled[:rows, :cols]

        # Flow Corrections
        expected_image = warp(b_gaussian_pyr[i], U_p, V_p, interpolation, border_mode)
        U_c, V_c = optic_flow_lk(a_gaussian_pyr[i], expected_image, k_size, k_type, sigma)

        # Add Flow Corrections to Predicted Flow
        U = U_p + U_c
        V = V_p + V_c

    return U, V
