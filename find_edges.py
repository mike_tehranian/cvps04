"""
Helper code to learn how an edge image is found and what the input parameters do.

How to run:
python find_edges.py <image path>

Try to think of other ways you can use a similar approach using different functions.
This way, tuning parameters with trackbars should be easier.
"""

import argparse
import cv2
import numpy as np
import os

import ps4

import guiutils
import guiutils_hlk


# I/O directories
input_dir = "input_images"
output_dir = "output"


def main(input_image):
    #img = cv2.imread(args.filename, cv2.IMREAD_GRAYSCALE)

    shift_0 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                      'Shift0.png'), 0) / 255.
    shift_r2 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                       'ShiftR2.png'), 0) / 255.
    shift_r5_u5 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                          'ShiftR5U5.png'), 0) / 255.
    shift_r10 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                        'ShiftR10.png'), 0) / 255.
    shift_r20 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                        'ShiftR20.png'), 0) / 255.
    shift_r40 = cv2.imread(os.path.join(input_dir, 'TestSeq',
                                        'ShiftR40.png'), 0) / 255.
    #cv2.imshow('input', shift_0)
    #cv2.waitKey(0)

    # Part 1 B
    k_size = 25 #TODO try different values
    sigma = 2 #TODO try different values
    #shift_0_blur = cv2.GaussianBlur(shift_0, (37, 37), 7)
    shift_0_blur = cv2.GaussianBlur(shift_0, (k_size, k_size), sigma)
    shift_r10_blur = cv2.GaussianBlur(shift_r10, (k_size, k_size), sigma)
    shift_r20_blur = cv2.GaussianBlur(shift_r20, (k_size, k_size), sigma)
    shift_r40_blur = cv2.GaussianBlur(shift_r40, (k_size, k_size), sigma)

    #edge_finder = guiutils.EdgeFinder(shift_0, shift_r40, filter_size=46,
                             #threshold1='uniform', threshold2=1, threshold3=k_size, threshold4=sigma)

    urban_img_01 = cv2.imread(
        os.path.join(input_dir, 'Urban2', 'urban01.png'), 0) / 255.
    urban_img_02 = cv2.imread(
        os.path.join(input_dir, 'Urban2', 'urban02.png'), 0) / 255.
    edge_finder = guiutils_hlk.EdgeFinder(urban_img_01, urban_img_02, filter_size=102,
                             threshold1='uniform', threshold2=1, threshold3=sigma)

    print "Edge parameters:"
    print "Filter Size: %f" % edge_finder.filterSize()
    print "Threshold1: %s" % edge_finder.threshold1()
    print "Threshold2: %f" % edge_finder.threshold2()
    print "Threshold3: %s" % edge_finder.threshold3()
    #print "Threshold4: %f" % edge_finder.threshold4()

    #(head, tail) = os.path.split(args.filename)

    #(root, ext) = os.path.splitext(tail)

    #smoothed_filename = os.path.join("output_images", root + "-smoothed" + ext)
    #edge_filename = os.path.join("output_images", root + "-edges" + ext)

    #cv2.imwrite(smoothed_filename, edge_finder.smoothedImage())
    #cv2.imwrite(edge_filename, edge_finder.edgeImage())

    cv2.destroyAllWindows()


def old_main():
    parser = argparse.ArgumentParser(description='Visualizes an edge image.')
    parser.add_argument('filename')

    args = parser.parse_args()

    img = cv2.imread(args.filename, cv2.IMREAD_GRAYSCALE)

    cv2.imshow('input', img)

    edge_finder = EdgeFinder(img, filter_size=13, threshold1=28, threshold2=115)

    print "Edge parameters:"
    print "GaussianBlur Filter Size: %f" % edge_finder.filterSize()
    print "Threshold1: %f" % edge_finder.threshold1()
    print "Threshold2: %f" % edge_finder.threshold2()

    (head, tail) = os.path.split(args.filename)

    (root, ext) = os.path.splitext(tail)

    smoothed_filename = os.path.join("output_images", root + "-smoothed" + ext)
    edge_filename = os.path.join("output_images", root + "-edges" + ext)

    cv2.imwrite(smoothed_filename, edge_finder.smoothedImage())
    cv2.imwrite(edge_filename, edge_finder.edgeImage())

    cv2.destroyAllWindows()


if __name__ == '__main__':
    main(None)
