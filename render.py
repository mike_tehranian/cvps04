import cv2
import numpy as np


# MDT: also worth checking out:
# http://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_gui/py_trackbar/py_trackbar.html


# As shown in example at bottom of code below, the class instantiation accepts:
# 1. function,
# 2. constants and
# 3. variables, where variables will be displayed as track bars.
# It surely helped me in tuning various outputs with varying degrees of freedom
class Render(object):
    def __init__(self, function, constants, variables):
        """Accepts
        @param function: which gets inputs 'constants' and 'variables'
        @param constants: to the function like images
        @param variables: dictionary of variables.
                            The keys are variables and values are max values. min is 0.
                            increments in steps of 1 from min->max
        """
        self.window_name = 'Param Tuning'

        self.function = function
        self.constants = constants
        self.variables = variables

        cv2.namedWindow(self.window_name)
        for varK, varV in self.variables.items():
            cv2.createTrackbar(varK, 'Param Tuning', 0, varV, self.execute)

    def execute(self, val=None):
        """Renders the window with original image and processed image with track bars"""
        # - Get the current positions on track bars
        for var in self.variables.keys():
            self.variables[var]=cv2.getTrackbarPos(var, self.window_name)
        self.variables['window']=self.window_name

        # - Execute function with current track bar values
        # self.function(**self.variables)
        try:
            print self.variables
            self.function(*self.constants, **self.variables)
        except np.linalg.LinAlgError:
            print 'Singular Matrix found...'
        except:
            print 'Some other error... CHECK...'

        cv2.waitKey(0)
        cv2.destroyAllWindows()


# USAGE Example:
# Tune_Motion_In_Image could be any function which accepts constants (e.g. images) and variables (variable names are keys of dictionary as shown below)
# Example Signature (can have any variable(s), but **make sure** to include 'window=None' in signature (the last argument as shown below)):
#
# def Tune_Motion_In_Image(img_a, img_b, outfile, k_size=11, k_type_val=1, sigma=5, k_gauss_img=3, sigma_img=2, window=None):
#       ... #(your logic)
#
#       **MUST** include following 2 lines in your desired function
#       windowName = 'result' if window is None else window
#       cv2.imshow(windowName, <output image you want to display with trackbars>)
#

variables = {
             'k_size': 3,
             'k_type_val': 1, # this is just a switch. I used inside my Tune_Motion_In_Image function as k_type="uniform" if k_type_val<0.5 else "gaussian"
             'sigma': 1,
             'k_gauss_img': 1,
             'sigma_img': 1
            }

tune_params = Render(Tune_Motion_In_Image, (shift_0, shift_r2, "ps4-1-a-1.png"), variables)
tune_params.execute()
